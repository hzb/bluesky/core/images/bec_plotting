FROM python:3.8-slim-bullseye
ENV TZ=Europe/Berlin

##### developer / build stage ##################################################

# Install essential build tools and utilities.
RUN apt update -y && apt upgrade -y
RUN apt install -y --no-install-recommends \
    git \
    libgl1-mesa-dri \
    libgl1-mesa-dri \
    tzdata \
    python3 \
    python3-pip \
    re2c \
    rsync \
    ssh-client \
    nano
    
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone  

RUN apt install -y libgl-dev libxkbcommon-dev libegl-dev libfontconfig-dev libdbus-1-dev \
    libxcb-*-dev libxkbcommon-x11-dev mesa-utils libgl1-mesa-glx
    
RUN pip install --upgrade pip
RUN python3 -m pip install PyQt5

# Install Python packages required for Bluesky and scientific computing.
RUN python3 -m pip install --upgrade bluesky matplotlib 
# Install packages for publisher
RUN python3 -m pip install apstools zmq

# Copy the startup files to the container
COPY src /opt/src

# Set the working directory to '/bluesky/'.
WORKDIR /opt/src/

ENTRYPOINT ["python3", "/opt/src/simple_plotting.py"]