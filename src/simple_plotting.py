import matplotlib.backends
from bluesky.callbacks.zmq import RemoteDispatcher
from bluesky.callbacks.best_effort import BestEffortCallback

import matplotlib.pyplot as plt
plt.ion()

bec = BestEffortCallback()
bec.enable_plots()
#bec.disable_table()
bec.disable_baseline()

from bluesky.utils import install_remote_qt_kicker

import os
ip = os.environ.get('HOST_IP')
port = os.environ.get('PORT')


d = RemoteDispatcher(ip + ':'+ port)
d.subscribe(bec)
install_remote_qt_kicker(loop=d.loop)

print(f"Connected to {ip}:{port} Ready to Plot, Ctrl + C to Exit")

d.start()