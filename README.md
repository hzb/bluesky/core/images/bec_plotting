### A Container for plotting

This image can be run and passed environment variables `HOST_IP` and `PORT`

It will then listen for documents from a RE over 0mq and plot the results. You can run the container like this:

first configure your host x11 environment
`xhost +local:docker`

Then run the container passing in the required variables. This will run in the background. If you want the shell, you can use the flag `-it`

`docker run -d --rm --name bluesky_plotting_callback --network host --env DISPLAY=${DISPLAY} --env HOST_IP=localhost --env PORT=5578 -v /tmp/.X11-unix:/tmp/.X11-unix -v /etc/localtime:/etc/localtime:ro -v /etc/timezone:/etc/timezone:ro --device=/dev/dri:/dev/dri registry.hzdr.de/hzb/bluesky/core/images/bec_plotting:1-0-0`

You can also run `./start.sh` if this is easier which will connect to a RE on your local machine forwarding it's documents over 0MQ at port 5578

### Configuring the RE

In this example we configure a RE to forward it's documents over 0MQ on port 5577
```
from bluesky import RunEngine
RE = RunEngine({})

from bluesky.callbacks.zmq import Publisher
publisher = Publisher('localhost:5577')
RE.subscribe(publisher)
```

You need to run a 0MQ buffer somewhere. The easiest way is in another shell with the command (assuming you're in a python environment with bluesky and 0MQ installed):

`bluesky-0MQ-proxy 5577 5578`

You can also start a container that 

`docker run -d --rm --name bluesky_zmq --network host registry.hzdr.de/hzb/bluesky/core/images/zmq-proxy:1-0-0 bluesky-0MQ-proxy 5577 5578`
